# STM32F407RCT6 SPI+DMA 驱动 ST7789 TFT 屏幕（含触摸）代码仓库

欢迎来到本代码仓库，这里提供的是一套详细的示例程序，旨在演示如何利用STM32F407RCT6微控制器的标准库通过SPI接口结合DMA传输方式来高效驱动ST7789型TFT液晶显示屏，并且支持触摸功能。ST7789是一款广泛应用在小型彩色LCD显示器上的图形显示器IC，适用于各种嵌入式系统和便携式设备。

## 特性

- **SPI + DMA**：利用DMA（直接存储器访问）减轻CPU负担，提高数据传输效率，适合高速显示更新。
- **标准库实现**：基于STM32的标准固件库编写，便于初学者理解学习，同时保持了良好的兼容性和可读性。
- **触摸支持**：集成触摸控制功能，允许用户进行交互操作，提升了应用的互动体验。
- **详细注释**：代码中包含详尽的注释，帮助开发者快速理解和修改以适应不同项目需求。
- **移植指南**：虽然主要针对STM32F407RCT6，但提供了基本的移植指南，有助于将此方案应用于其他STM32系列芯片。

## 快速入门

1. **环境搭建**：
   - 确保你拥有STM32CubeMX或类似工具来配置你的STM32开发板。
   - 使用Keil、IAR或STM32CubeIDE等编译器准备编译环境。

2. **配置步骤**：
   - 在STM32CubeMX中配置SPI和DMA，确保与硬件连接一致。
   - 生成工程并导入到你选择的IDE中。
   
3. **代码整合**：
   - 将本仓库中的源代码文件复制到你的项目中。
   - 根据实际硬件调整必要的参数，如SPI时钟速度、屏幕分辨率等。

4. **测试运行**：
   - 编译并烧录代码到STM32板上。
   - 连接屏幕，检查显示是否正常以及触摸响应情况。

## 注意事项

- **硬件匹配**：请确认你的屏幕型号是ST7789，并检查其引脚排列与本代码假设的一致。
- **调试技巧**：若遇到问题，首先检查硬件连接，然后通过串口日志辅助调试。
- **版本兼容性**：确保所使用的开发环境与代码库兼容，尤其是库函数和编译器版本。

## 开源协议

本代码遵循[MIT开源协议](https://opensource.org/licenses/MIT)，鼓励分享与再创造。在使用过程中，请尊重原作者的劳动成果，适当引用或贡献代码改进。

## 社区与支持

对于技术疑问或交流心得，欢迎参与相关论坛或社区讨论。记住，共享知识是进步的动力！

通过本仓库的学习和实践，你将能够掌握STM32使用SPI+DMA技术驱动TFT屏幕及实现触摸功能的核心技能。祝你在嵌入式开发的道路上越走越远，探索更多的可能！